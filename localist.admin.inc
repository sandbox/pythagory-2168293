<?php
/**
 * @file
 * Admin page callback for Localist block settings
 */

/**
 * Helper function for building lists of filter options
 * @param  object $filters     json-decoded object with filter information
 * @param  string $filter_type Name of the filter to build a list of
 * @return array               Array of available filters to pass to Drupal forms API
 */
function buildFilterList($filters, $filter_type) {
  $parent_list = array(); // List of top-level items
  $filter_list = array(); // Final list of filters that we'll be returning

  // Get 'parent' items first
  foreach($filters->$filter_type as $filter) {
    if(is_null($filter->parent_id)) {
      $parent_list[$filter->id] = $filter->name;
    }
  }

  // Alphabetically sort list
  asort($parent_list);

  // Iterate over parents and fetch children
  foreach ($parent_list as $parent_id => $parent_name) {
    $children = array();
    
    foreach ($filters->$filter_type as $filter) {
      if($filter->parent_id == $parent_id) {
        $children[$filter->id] = '- ' . $filter->name;
      }
    }

    asort($children);

    // Add parent with children to final list
    $filter_list = $filter_list + array($parent_id => $parent_name) + $children;

    unset($children);
  }

  // Prepend 'Any' value and return
  return array('0' => 'Any') + $filter_list;
}

/**
 * Builds Localist settings page
 * @return array Output of system_settings_form() containing form structure
 */
function localist_admin_settings() {
  // Fetch current settings in an array
  $localist_settings = variable_get('localist_settings', array());

  // Basic form setup
  $form['localist_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Localist Block Settings'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );

  // Calendar URL field
  $form['localist_settings']['localist_calendar_url'] = array(
    '#title' => t('Localist Calendar URL'),
    '#description' => t('e.g. http://calendar.mysite.org'),
    '#type' => 'textfield',
    '#maxlength' => 64,
    '#size' => 64,
    '#default_value' => (!empty($localist_settings['localist_calendar_url']))
                        ? $localist_settings['localist_calendar_url']
                        : '',
    '#required' => TRUE,
  );

  // Display quantity
  $form['localist_settings']['localist_display_quantity'] = array(
    '#title' => t('Number of Events to Show'),
    '#description' => t('Maximum: 100'),
    '#type' => 'textfield',
    '#maxlength' => 3,
    '#size' => 6,
    '#default_value' => (!empty($localist_settings['localist_display_quantity']))
                        ? $localist_settings['localist_display_quantity']
                        : '10',
  );

  // Days into the future
  $form['localist_settings']['localist_days_ahead'] = array(
    '#title' => t('Show events for the next n days'),
    '#description' => t('Maximum: 365'),
    '#type' => 'textfield',
    '#maxlength' => 3,
    '#size' => 6,
    '#default_value' => (!empty($localist_settings['localist_days_ahead']))
                        ? $localist_settings['localist_days_ahead']
                        : '1',
  );

  // Sort option
  $form['localist_settings']['localist_sort_option'] = array(
    '#title' => t('Sort results by'),
    '#type' => 'select',
    '#options' => array(
                        'date' => 'Event Date',
                        'name' => 'Event Name',
                        'ranking' => 'Event Popularity',
                        'created' => 'Created Date',
                        'updated' => 'Updated Date'
                       ),
    '#default_value' => (!empty($localist_settings['localist_sort_option']))
                    ? $localist_settings['localist_sort_option']
                    : 'date',
  );

  // Distinct events flag
  $form['localist_settings']['localist_distinct_events'] = array(
    '#title' => t('Only Show First Instance of Events'),
    '#type' => 'checkbox',
    '#default_value' => (!empty($localist_settings['localist_distinct_events']))
                        ? $localist_settings['localist_distinct_events']
                        : 'checked',
  );  

  // Featured events flag
  $form['localist_settings']['localist_featured_events'] = array(
    '#title' => t('Only Show Featured Events'),
    '#type' => 'checkbox',
    '#default_value' => (!empty($localist_settings['localist_featured_events']))
                        ? $localist_settings['localist_featured_events']
                        : 'checked',
  );

  /* Localist-provided filters
   * Localist provides a handfull of filters for departments, locations, etc.
   * Here, we'll fetch that list and process them into user selects.
   */
  $filter_departments = array();
  $filter_event_types = array();
  $filter_event_audiences = array();

  if(!empty($localist_settings['localist_calendar_url'])) {
    // Fetch list of available filters
    $url = sprintf('%s/api/2/events/filters', $localist_settings['localist_calendar_url']);
    $options = array();

    $raw_result = drupal_http_request($url, $options);
    $filters = json_decode($raw_result->data);

    // Unpack results into key/value pairs
    $filter_departments = buildFilterList($filters, 'departments');

    $filter_event_types = buildFilterList($filters, 'event_types');

    $filter_event_audiences = buildFilterList($filters, 'event_target_audience');
  }

  // Department Filter
  $form['localist_settings']['localist_filter_department'] = array(
    '#title' => t('Department'),
    '#type' => 'select',
    '#options' => $filter_departments,
    '#default_value' => (!empty($localist_settings['localist_filter_department']))
                        ? $localist_settings['localist_filter_department']
                        : 0,
  );

  // Event Type Filter
  $form['localist_settings']['localist_filter_event_type'] = array(
    '#title' => t('Event Type'),
    '#type' => 'select',
    '#options' => $filter_event_types,
    '#default_value' => (!empty($localist_settings['localist_filter_event_type']))
                        ? $localist_settings['localist_filter_event_type']
                        : 0,
  );

  // Event Target Audience Filter
  $form['localist_settings']['localist_filter_target_audience'] = array(
    '#title' => t('Event Target Audience'),
    '#type' => 'select',
    '#options' => $filter_event_audiences,
    '#default_value' => (!empty($localist_settings['localist_filter_target_audience']))
                        ? $localist_settings['localist_filter_target_audience']
                        : 0,
  );

  return system_settings_form($form);
}